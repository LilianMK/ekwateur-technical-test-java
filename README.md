# ekWateur Technical Test Java
Programme de calcul des factures d'énergie

# Implémentation des clients
La classe Client contient la référence client et vérifie à la construction si cette référence est valide.
Les classes ClientPro et ClientParticulier étendent cette classe Client en ajoutant les attributs spécifiques (SIRET, CA, Raison sociale pour les entreprises, nom, prénom, civilité pour les particuliers) ainsi que les vérifications si nécessaires.

# Calcul de la facture
La classe Facture sert à calculer le montant des consommations d'énergie d'un client sur un mois. Le calcul s'effectue en fonction du type de client et utilise les constantes définies dans l'énoncé.
Le prix est arrondi au centime.

# Tests
Les tests se trouvent dans le main de la classe Test.