public abstract class Client {
    protected final String referenceClient;

    public Client(String referenceClient) {
        isCorrectRef(referenceClient);
        this.referenceClient = referenceClient;
    }

    /**
     * Vérification que la référence soit de la forme EKWxxxxxxxx avec x des chiffres
     * @param ref : référence client à vérifier
     */
    private static void isCorrectRef(String ref){
        if (ref.length() != 11) throw new IllegalArgumentException("La référence client doit avoir 11 caractères.");
        if (!ref.startsWith("EKW")) throw new IllegalArgumentException("La référence client doit commencer par \"EKW\".");
        for (int i = 3; i < 11; i++){
            if (!Character.isDigit(ref.charAt(i))) throw new IllegalArgumentException("La référence client ne doit contenir uniquement des chiffres après \"EKW\".");
        }
    }
}