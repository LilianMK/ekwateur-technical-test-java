public class ClientPro extends Client{
    private final String siret;
    private final String raisonSociale;
    private double ca;

    public ClientPro(String referenceClient, String siret, String raisonSociale, double ca) {
        super(referenceClient);
        isCorrectSiret(siret);
        if (ca < 0) throw new IllegalArgumentException("Le chiffre d'affaire ne peut pas être négatif.");
        this.siret = siret;
        this.raisonSociale = raisonSociale;
        this.ca = ca;
    }

    public double getCa() { return ca; }

    /**
     * Vérification du Siret (doit contenir 14 chiffres)
     * @param siret : siret à vérifier
     */
    private static void isCorrectSiret(String siret){
        if (siret.length() != 14) throw new IllegalArgumentException("Le siret doit contenir 14 chiffres.");
        if (!siret.matches("\\d+")) throw new IllegalArgumentException("Le siret doit être composé uniquement de chiffres.");
    }

    @Override
    public String toString() {
        return raisonSociale + " (n° client : " + referenceClient + ", SIRET : " + siret + ")";
    }
}
