import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.YearMonth;

public class Facture {
    // Constantes des prix selon le type de client
    private static final double PRIX_ELECTRICITE_PARTICULIER = 0.121;
    private static final double PRIX_GAZ_PARTICULIER = 0.115;
    private static final double PRIX_ELECTRICITE_MICRO_CA = 0.118;
    private static final double PRIX_GAZ_MICRO_CA = 0.113;
    private static final double PRIX_ELECTRICITE_GROS_CA = 0.114;
    private static final double PRIX_GAZ_GROS_CA = 0.111;

    private final Client client;
    private final double consommationElec;
    private final double consommationGaz;
    private final YearMonth yearMonth;
    private final double montant;

    /**
     * Constructeur de Facture
     *
     * @param client      : client cible de la facture
     * @param consommationElec : consommation du client d'électricité en kWh
     * @param consommationGaz : consommation du client de gaz en kWh
     * @param mois        : mois de la facture
     * @param annee       : année de la facture
     */
    public Facture(Client client, double consommationElec, double consommationGaz, int mois, int annee) {
        if (consommationElec < 0 || consommationGaz < 0) throw new IllegalArgumentException("Les consommations doivent être nulles ou positives.");
        this.client = client;
        this.consommationElec = consommationElec;
        this.consommationGaz = consommationGaz;
        this.yearMonth = YearMonth.of(annee, mois);
        this.montant = arrondirCentime(calcul());
    }

    /**
     * Fonction qui calcul pour un client le montant dû selon le type de client, d'énergie et la consommation
     * @return Montant dû par le client pour le mois
     */
    private double calcul() {
        double prix;
        if (client instanceof ClientParticulier){
            prix = consommationElec * PRIX_ELECTRICITE_PARTICULIER + consommationGaz * PRIX_GAZ_PARTICULIER;
        } else if (((ClientPro) client).getCa() > 1_000_000){
            prix = consommationElec * PRIX_ELECTRICITE_GROS_CA + consommationGaz * PRIX_GAZ_GROS_CA;
        } else {
            prix = consommationElec * PRIX_ELECTRICITE_MICRO_CA + consommationGaz * PRIX_GAZ_MICRO_CA;
        }
        return prix;
    }

    /**
     * Arrondie le prix au centime près
     * @param prix : prix d'origine
     * @return : prix arrondi
     */
    private double arrondirCentime(double prix){
        BigDecimal prixArrondi = new BigDecimal(prix);
        prixArrondi = prixArrondi.setScale(2, RoundingMode.HALF_UP);
        return prixArrondi.doubleValue();
    }

    @Override
    public String toString() {
        return "[" + client + "] : Facture de " + yearMonth + "\n"
                + "Consommation d'électricité : " + consommationElec + " kWh\n"
                + "Consommation de gaz : " + consommationGaz + " kWh\n"
                + "Montant : " + montant + "€";
    }
}

