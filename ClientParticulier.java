public class ClientParticulier extends Client{

    enum Civilite{
        MR("M. "),
        MME("Mme "),
        AUTRE("");

        private final String label;

        Civilite(String label){
            this.label = label;
        }
    }
    private final String nom;
    private final String prenom;
    private final Civilite civilite;

    public ClientParticulier(String referenceClient, String nom, String prenom, Civilite civilite) {
        super(referenceClient);
        this.nom = nom;
        this.prenom = prenom;
        this.civilite = civilite;
    }

    @Override
    public String toString() {
        return civilite.label + prenom + " " + nom + " (n° client : " + referenceClient + ")";
    }
}
