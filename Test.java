public class Test {

    public static void main(String[] args) {

        ClientParticulier clientParticulier = new ClientParticulier("EKW12345678", "Bombeur", "Jean", ClientParticulier.Civilite.MR);
        ClientPro clientProGros = new ClientPro("EKW11235813", "16180339887498", "Abstergo Industries", 10_572_391);
        ClientPro clientProPetit = new ClientPro("EKW31415926", "26710024561337", "Aperture Science", 756_235);

        Facture factureParticulier = new Facture(clientParticulier, 82.3, 242.5, 5, 2023);
        Facture factureProGros = new Facture(clientProGros, 745.1, 2103, 4, 2022);
        Facture factureProPetit = new Facture(clientProPetit, 202.3, 1563.7, 6, 2022);
        System.out.println(factureParticulier);
        System.out.println(factureProGros);
        System.out.println(factureProPetit);

        try{
            ClientParticulier refTropCourte = new ClientParticulier("EKW131122", "Menvussa", "Gerard", ClientParticulier.Civilite.AUTRE);
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        try{
            ClientParticulier refEKW = new ClientParticulier("EK454689123", "Dorsa", "Elsa", ClientParticulier.Civilite.MME);
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        try{
            ClientPro refDigit = new ClientPro("EKW123L4567", "47134870950035", "Black Mesa", 9_000_000);
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        try{
            ClientPro siretCourt = new ClientPro("EKW15975345", "389905761000", "Nivento",  565_531_053);
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        try{
            ClientPro siretDigit = new ClientPro("EKW14965732", "51536782O49133", "Umbrella Corporation", 513_000_000);
        }catch(IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        try{
            ClientPro mauvaisCa = new ClientPro("EKW87654321", "93965294850143", "Shinra Electric Power Company", -8623);
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

        try{
            Facture mauvaiseConso = new Facture(clientParticulier, -42, 0, 12, 2012);
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
    }
}
